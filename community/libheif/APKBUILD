# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=libheif
pkgver=1.15.2
pkgrel=1
pkgdesc="ISO/IEC 23008-12:2017 HEIF file format decoder and encoder"
url="https://www.libde265.org/"
arch="all"
license="LGPL-3.0-or-later"
depends_dev="aom-dev x265-dev libde265-dev"
makedepends="$depends_dev libjpeg-turbo-dev libpng-dev gdk-pixbuf-dev"
options="!check"  # no tests provided
subpackages="
	$pkgname-dev
	$pkgname-tools
	$pkgname-doc
	$pkgname-gdk-pixbuf:gdk_pixbuf
	"
source="https://github.com/strukturag/libheif/releases/download/v$pkgver/libheif-$pkgver.tar.gz"

# secfixes:
#   1.5.0-r0:
#     - CVE-2019-11471

build() {
	# This is en/decoder, so performance matters more than size.
	export CPPFLAGS="${CFLAGS/-Os/-O2}"
	export CXXFLAGS="${CFLAGS/-Os/-O2} -flto=auto"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-static \
		--disable-go
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="$pkgdesc (tools)"

	amove usr/bin
}

gdk_pixbuf() {
	amove usr/lib/gdk-pixbuf-*/*/loaders
}

sha512sums="
79f22dad084bbe965dae7b8f92fc119dbdde7a04577ca9c7bc28b1a022a06f0ae64d3fbd11e82d6907f0fd678a31d98275e508a51a6bd0a9e8675c93790f1996  libheif-1.15.2.tar.gz
"
