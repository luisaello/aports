From 44d63e2d4e4a3cba3d0d49b452782328cf6fc393 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Pablo=20Correa=20G=C3=B3mez?= <ablocorrea@hotmail.com>
Date: Mon, 12 Dec 2022 17:44:51 +0100
Subject: [PATCH] network: keep track of radio buttons in connection editor
 with action

Hooking to all the toggled signals from all the buttons for executing
the same action is inneficient, and can potenticall end up in a segmentation
 fault due to some race in the signal emmission, where the active button
 gets deactivated before the clicked button is activated

Looking at the GTK4 code, in a radio group:

- The button which was previously active gets de-activated, emitting its
corresponding toggled signal.
- The active property for the clicked button gets set.
- The clicked button emits its toggled signal.

Therefore, if the first toggle signal gets processed before the active
property is set, there can be a race condition. We are seeing this downstream
at pmOS: https://gitlab.com/postmarketOS/pmaports/-/issues/1816

Additionally, make sure we throw a critical for the unreachable code.
---
 .../network/connection-editor/ce-page-ip4.c   | 26 ++++++++++++++----
 .../network/connection-editor/ce-page-ip6.c   | 27 ++++++++++++++-----
 panels/network/connection-editor/ip4-page.ui  | 14 +++++++---
 panels/network/connection-editor/ip6-page.ui  | 17 ++++++++----
 4 files changed, 64 insertions(+), 20 deletions(-)

diff --git a/panels/network/connection-editor/ce-page-ip4.c b/panels/network/connection-editor/ce-page-ip4.c
index c3f0864d9f..6e5e2eb89b 100644
--- a/panels/network/connection-editor/ce-page-ip4.c
+++ b/panels/network/connection-editor/ce-page-ip4.c
@@ -512,11 +512,6 @@ connect_ip4_page (CEPageIP4 *self)
                                      nm_setting_ip_config_get_never_default (self->setting));
         g_signal_connect_object (self->never_default_check, "toggled", G_CALLBACK (ce_page_changed), self, G_CONNECT_SWAPPED);
 
-        g_signal_connect_object (self->automatic_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-        g_signal_connect_object (self->local_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-        g_signal_connect_object (self->manual_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-        g_signal_connect_object (self->disabled_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-
         switch (method) {
         case IP4_METHOD_AUTO:
                 gtk_check_button_set_active (GTK_CHECK_BUTTON (self->automatic_radio), TRUE);
@@ -569,6 +564,8 @@ ui_to_setting (CEPageIP4 *self)
                 method = NM_SETTING_IP4_CONFIG_METHOD_MANUAL;
         else if (gtk_check_button_get_active (GTK_CHECK_BUTTON (self->shared_radio)))
                 method = NM_SETTING_IP4_CONFIG_METHOD_SHARED;
+        else
+                g_assert_not_reached ();
 
         addresses = g_ptr_array_new_with_free_func ((GDestroyNotify) nm_ip_address_unref);
         add_addresses = g_str_equal (method, NM_SETTING_IP4_CONFIG_METHOD_MANUAL);
@@ -764,6 +761,17 @@ out:
         return ret;
 }
 
+static void
+on_ip4_method_activated_cb (GSimpleAction* action,
+                            GVariant* parameter,
+                            gpointer user_data)
+{
+        CEPageIP4 *self = CE_PAGE_IP4 (user_data);
+        g_simple_action_set_state (action, parameter);
+
+        method_changed (self);
+}
+
 static const gchar *
 ce_page_ip4_get_title (CEPage *page)
 {
@@ -784,6 +792,14 @@ ce_page_ip4_validate (CEPage        *self,
 static void
 ce_page_ip4_init (CEPageIP4 *self)
 {
+        const GActionEntry ip4_entries[] = {
+                { "ip4method", on_ip4_method_activated_cb, "s", "'automatic'", NULL},
+        };
+        GSimpleActionGroup *group = g_simple_action_group_new ();
+
+        g_action_map_add_action_entries (G_ACTION_MAP (group), ip4_entries, G_N_ELEMENTS (ip4_entries), self);
+        gtk_widget_insert_action_group (GTK_WIDGET (self), "ip4page", G_ACTION_GROUP (group));
+
         gtk_widget_init_template (GTK_WIDGET (self));
 }
 
diff --git a/panels/network/connection-editor/ce-page-ip6.c b/panels/network/connection-editor/ce-page-ip6.c
index 09b9eb6f52..3f2efb9142 100644
--- a/panels/network/connection-editor/ce-page-ip6.c
+++ b/panels/network/connection-editor/ce-page-ip6.c
@@ -485,12 +485,6 @@ connect_ip6_page (CEPageIP6 *self)
                                      nm_setting_ip_config_get_never_default (self->setting));
         g_signal_connect_object (self->never_default_check, "toggled", G_CALLBACK (ce_page_changed), self, G_CONNECT_SWAPPED);
 
-        g_signal_connect_object (self->automatic_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-        g_signal_connect_object (self->dhcp_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-        g_signal_connect_object (self->local_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-        g_signal_connect_object (self->manual_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-        g_signal_connect_object (self->disabled_radio, "toggled", G_CALLBACK (method_changed), self, G_CONNECT_SWAPPED);
-
         switch (method) {
         case IP6_METHOD_AUTO:
                 gtk_check_button_set_active (GTK_CHECK_BUTTON (self->automatic_radio), TRUE);
@@ -544,6 +538,8 @@ ui_to_setting (CEPageIP6 *self)
                 method = NM_SETTING_IP6_CONFIG_METHOD_AUTO;
         else if (gtk_check_button_get_active (self->shared_radio))
                 method = NM_SETTING_IP6_CONFIG_METHOD_SHARED;
+        else
+                g_assert_not_reached ();
 
         nm_setting_ip_config_clear_addresses (self->setting);
         if (g_str_equal (method, NM_SETTING_IP6_CONFIG_METHOD_MANUAL)) {
@@ -734,6 +730,17 @@ out:
         return ret;
 }
 
+static void
+on_ip6_method_activated_cb (GSimpleAction* action,
+                            GVariant* parameter,
+                            gpointer user_data)
+{
+        CEPageIP6 *self = CE_PAGE_IP6 (user_data);
+        g_simple_action_set_state (action, parameter);
+
+        method_changed (self);
+}
+
 static const gchar *
 ce_page_ip6_get_title (CEPage *page)
 {
@@ -754,6 +761,14 @@ ce_page_ip6_validate (CEPage        *self,
 static void
 ce_page_ip6_init (CEPageIP6 *self)
 {
+        const GActionEntry ip6_entries[] = {
+                { "ip6method", on_ip6_method_activated_cb, "s", "'automatic'", NULL},
+        };
+        GSimpleActionGroup *group = g_simple_action_group_new ();
+
+        g_action_map_add_action_entries (G_ACTION_MAP (group), ip6_entries, G_N_ELEMENTS (ip6_entries), self);
+        gtk_widget_insert_action_group (GTK_WIDGET (self), "ip6page", G_ACTION_GROUP (group));
+
         gtk_widget_init_template (GTK_WIDGET (self));
 }
 
diff --git a/panels/network/connection-editor/ip4-page.ui b/panels/network/connection-editor/ip4-page.ui
index a2210ea8ad..c03b644d74 100644
--- a/panels/network/connection-editor/ip4-page.ui
+++ b/panels/network/connection-editor/ip4-page.ui
@@ -34,6 +34,8 @@
                 <child>
                   <object class="GtkCheckButton" id="automatic_radio">
                     <property name="label" translatable="yes">Automatic (DHCP)</property>
+                    <property name="action-name">ip4page.ip4method</property>
+                    <property name="action-target">'automatic'</property>
                     <layout>
                       <property name="row">0</property>
                       <property name="column">1</property>
@@ -43,7 +45,8 @@
                 <child>
                   <object class="GtkCheckButton" id="local_radio">
                     <property name="label" translatable="yes">Link-Local Only</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip4page.ip4method</property>
+                    <property name="action-target">'local'</property>
                     <layout>
                       <property name="row">0</property>
                       <property name="column">2</property>
@@ -53,7 +56,8 @@
                 <child>
                   <object class="GtkCheckButton" id="manual_radio">
                     <property name="label" translatable="yes">Manual</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip4page.ip4method</property>
+                    <property name="action-target">'manual'</property>
                     <layout>
                       <property name="row">1</property>
                       <property name="column">1</property>
@@ -63,7 +67,8 @@
                 <child>
                   <object class="GtkCheckButton" id="disabled_radio">
                     <property name="label" translatable="yes">Disable</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip4page.ip4method</property>
+                    <property name="action-target">'disabled'</property>
                     <layout>
                       <property name="row">1</property>
                       <property name="column">2</property>
@@ -73,7 +78,8 @@
                 <child>
                   <object class="GtkCheckButton" id="shared_radio">
                     <property name="label" translatable="yes">Shared to other computers</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip4page.ip4method</property>
+                    <property name="action-target">'shared'</property>
                     <layout>
                       <property name="row">2</property>
                       <property name="column">1</property>
diff --git a/panels/network/connection-editor/ip6-page.ui b/panels/network/connection-editor/ip6-page.ui
index c22fb0f9e4..c4817ceacf 100644
--- a/panels/network/connection-editor/ip6-page.ui
+++ b/panels/network/connection-editor/ip6-page.ui
@@ -34,6 +34,8 @@
                 <child>
                   <object class="GtkCheckButton" id="automatic_radio">
                     <property name="label" translatable="yes">Automatic</property>
+                    <property name="action-name">ip6page.ip6method</property>
+                    <property name="action-target">'automatic'</property>
                     <layout>
                       <property name="row">0</property>
                       <property name="column">1</property>
@@ -43,7 +45,8 @@
                 <child>
                   <object class="GtkCheckButton" id="dhcp_radio">
                     <property name="label" translatable="yes">Automatic, DHCP only</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip6page.ip6method</property>
+                    <property name="action-target">'dhcp'</property>
                     <layout>
                       <property name="row">0</property>
                       <property name="column">2</property>
@@ -53,7 +56,8 @@
                 <child>
                   <object class="GtkCheckButton" id="local_radio">
                     <property name="label" translatable="yes">Link-Local Only</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip6page.ip6method</property>
+                    <property name="action-target">'local'</property>
                     <layout>
                       <property name="row">1</property>
                       <property name="column">1</property>
@@ -63,7 +67,8 @@
                 <child>
                   <object class="GtkCheckButton" id="manual_radio">
                     <property name="label" translatable="yes">Manual</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip6page.ip6method</property>
+                    <property name="action-target">'manual'</property>
                     <layout>
                       <property name="row">1</property>
                       <property name="column">2</property>
@@ -73,7 +78,8 @@
                 <child>
                   <object class="GtkCheckButton" id="disabled_radio">
                     <property name="label" translatable="yes">Disable</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip6page.ip6method</property>
+                    <property name="action-target">'disabled'</property>
                     <layout>
                       <property name="row">2</property>
                       <property name="column">1</property>
@@ -83,7 +89,8 @@
                 <child>
                   <object class="GtkCheckButton" id="shared_radio">
                     <property name="label" translatable="yes">Shared to other computers</property>
-                    <property name="group">automatic_radio</property>
+                    <property name="action-name">ip6page.ip6method</property>
+                    <property name="action-target">'shared'</property>
                     <layout>
                       <property name="row">2</property>
                       <property name="column">2</property>
-- 
GitLab

