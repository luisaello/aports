# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-calendar
pkgver=44.0
pkgrel=0
pkgdesc="Calendar application for GNOME"
url="https://wiki.gnome.org/Apps/Calendar"
# s390x and riscv64 blocked by gtk4
arch="all !s390x !riscv64"
license="GPL-3.0-or-later"
depends="gsettings-desktop-schemas"
makedepends="
	evolution-data-server-dev
	geoclue-dev
	geocode-glib-dev>=3.26.4-r1
	glib-dev
	gsettings-desktop-schemas-dev
	gtk4.0-dev
	libadwaita-dev
	libical-dev
	libsoup-dev
	libgweather4-dev
	meson
	"
options="!check" # Can't be run on release builds and we don't want debug asserts
subpackages="$pkgname-lang"
source="https://download.gnome.org/sources/gnome-calendar/${pkgver%.*}/gnome-calendar-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
38fd14f3361e8528b5671ed2019a13176e43aca10d47fcfdc9dff5ce8dc7e697245ad29f9d3572bc34fcebd0f3e351f15cd04f334e17c1d7450a4d128eda6355  gnome-calendar-44.0.tar.xz
"
