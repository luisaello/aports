# Contributor: Adam Plumb <adamplumb@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=libadwaita
pkgver=1.3.1
pkgrel=0
pkgdesc="Building blocks for modern GNOME applications"
url="https://gitlab.gnome.org/GNOME/libadwaita"
arch="all"
license="LGPL-2.1-or-later"
makedepends="meson sassc gtk4.0-dev gobject-introspection-dev vala"
checkdepends="dbus mesa-dri-gallium xvfb-run font-dejavu" # needs any font
subpackages="$pkgname-dev $pkgname-lang $pkgname-demo"
source="https://gitlab.gnome.org/GNOME/libadwaita/-/archive/$pkgver/libadwaita-$pkgver.tar.gz"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dexamples=true \
		-Dgtk_doc=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	XDG_RUNTIME_DIR="$(mktemp -d -p "$builddir")" \
	dbus-run-session -- \
	xvfb-run -a \
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

demo() {
	pkgdesc="$pkgdesc (libadwaita demo)"
	amove usr/share usr/bin
}

sha512sums="
03f7829ab8e3d27a1e7ced80d912a9087985534f17d5da5661aa985f9687b519957c2eca10ee7f4fb7262806e7d36478f0ab1f15f13cee435114b77de62e3128  libadwaita-1.3.1.tar.gz
"
