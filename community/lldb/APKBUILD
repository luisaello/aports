# Contributor: Bartłomiej Piotrowski <bpiotrowski@alpinelinux.org>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=lldb
# Note: Update together with llvm.
pkgver=16.0.1
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="Next generation, high-performance debugger"
# riscv64: https://github.com/llvm/llvm-project/issues/55383
arch="all !riscv64"
url="https://llvm.org/"
license="Apache-2.0"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	clang-dev>=$_llvmver
	clang-static>=$_llvmver
	cmake
	doxygen
	gtest-dev
	libedit-dev
	libffi-dev
	libxml2-dev
	linux-headers
	llvm$_llvmver-dev
	llvm$_llvmver-static
	ncurses-dev
	py3-sphinx
	python3-dev
	samurai
	swig
	xz
	"
subpackages="$pkgname-dev $pkgname-doc py3-$pkgname:py3"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/lldb-${pkgver//_/}.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/cmake-${pkgver//_/}.src.tar.xz
	atomic.patch
	fix-embedded_interpreter.patch
	"
builddir="$srcdir/$pkgname-${pkgver//_/}.src"
options="!check"  # FIXME: enable tests

prepare() {
	default_prepare
	mv "$srcdir"/cmake-${pkgver//_/}.src "$srcdir"/cmake
}

build() {
	export CC=clang
	export CXX=clang++

	case "$CARCH" in
	arm*)
		# binutils bugs
		;;
	*)
		export CFLAGS="$CFLAGS -flto"
		export CXXFLAGS="$CXXFLAGS -flto"
		;;
	esac

	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_SKIP_RPATH=ON \
		-DLLDB_BUILT_STANDALONE=ON \
		-DLLDB_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLLVM_ENABLE_SPHINX=ON \
		-DLLVM_LINK_LLVM_DYLIB=ON \
		-DCMAKE_POSITION_INDEPENDENT_CODE=ON

	cmake --build build --target all docs-lldb-man
}

check() {
	ninja -C build check-lldb
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 build/docs/man/lldb.1 -t "$pkgdir"/usr/share/man/man1
}

dev() {
	default_dev

	# move back main symlink, because py3-lldb links to it
	mv "$subpkgdir"/usr/lib/liblldb.so "$pkgdir"/usr/lib/
}

py3() {
	pkgdesc="Python3 module for LLDB"
	depends="$pkgname=$pkgver-r$pkgrel python3"
	replaces="py-lldb"

	amove usr/lib/python3.*
	python3 -m compileall -fq "$subpkgdir"/usr/lib/python3*
}

sha512sums="
1691f8b25e76fa070a3ff4a6b13675e6af9926a5b9039052969a7881f1f478777f92c214df69847d958ae67275b4ff05568420cbc39a2ed5bf8c24b436bac568  lldb-16.0.1.src.tar.xz
7a0f01a82660a953e06b1df267918542205ac46f151d284dca74255c36fe5ccd995266638a85ce502a81cdd5e76ff3311e5f8488e09d0abbf22ee07cec186f5f  cmake-16.0.1.src.tar.xz
6493c9bbcaf3f99ffbb56967147716bc93aa13b6c525214abc22723162bf42d6c25663ed1e2e11f432a8c1b246a528149e240eae027ce752bc20cc928ed57272  atomic.patch
fcd3f06b5f901d635bfe8edffa3951d5af402738ee55b13a98c992725eb35db17b7a3ca566eb94d48cd699be899f7402ae41da0d2f1ed4611afc5858a0eec561  fix-embedded_interpreter.patch
"
