# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-xarray
_pkgorig=xarray
pkgver=2023.4.0
pkgrel=0
pkgdesc="N-D labeled arrays and datasets in Python"
url="https://xarray.dev"
arch="noarch !s390x" # assertionErrors
license="Apache-2.0"
depends="python3 py3-numpy py3-packaging py3-pandas"
makedepends="python3-dev py3-setuptools_scm"
checkdepends="py3-coverage py3-mock py3-pytest py3-pytest-cov"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/x/xarray/xarray-$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest -k 'not test_dataset and not test_distributed and not test_dataarray'
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/xarray/tests
}

sha512sums="
1bb5052cb48e4ecb2d4e9d88cb0a0d3b25469b762d34059b5f65a88c2cbd125711d9af8a46a16b3eff3783f7d7653118b44503f19512ebc89845449a92efe1d9  py3-xarray-2023.4.0.tar.gz
"
