# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=isoimagewriter
pkgver=0.9.2
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/ISOImageWriter"
pkgdesc="A program to write hybrid ISO files onto a USB disk"
license="GPL-3.0-only"
makedepends="
	eudev-dev
	extra-cmake-modules
	gpgme-dev
	kauth-dev
	kcoreaddons-dev
	kcrash-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"
source="https://download.kde.org/unstable/isoimagewriter/$pkgver/isoimagewriter-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
17c5d5812c7d5a0284b6a5d1f318c26f1c3b1e80407cbe1e6acc22b593c4548f8ec52840c031e2771fb2c1d04746bd11416561f3c0c72b9c018f608a0eaae3c7  isoimagewriter-0.9.2.tar.xz
"
