# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=git-machete
pkgver=3.16.3
pkgrel=0
pkgdesc="git repository organizer & rebase/merge workflow automation tool"
url="https://github.com/VirtusLab/git-machete"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-mock"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/VirtusLab/git-machete/archive/refs/tags/v$pkgver/git-machete-$pkgver.tar.gz
	"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm0644 "$builddir"/completion/git-machete.completion.bash \
		"$pkgdir/usr/share/bash-completion/completions/git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.completion.zsh \
		"$pkgdir/usr/share/zsh/site-functions/_git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.fish \
		-t "$pkgdir/usr/share/fish/completions"
}

sha512sums="
fcd856c0fefecd2cf97ba149f1d1df57eb6fab0345c00c3d3df763041cc86226e3f815dda7c0050f8669d7d082e329dd76304de2092c7b43219e0c50a2e3467a  git-machete-3.16.3.tar.gz
"
