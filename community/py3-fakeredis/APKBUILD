# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=py3-fakeredis
_pkgname=${pkgname#py3-}
pkgver=2.10.3
pkgrel=0
pkgdesc="Fake implementation of redis API for testing purposes"
url="https://pypi.org/project/fakeredis/"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-redis py3-sortedcontainers"
makedepends="py3-poetry-core py3-gpep517"
checkdepends="py3-hypothesis py3-pytest py3-pytest-mock py3-lupa"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # no tests in tarball

prepare() {
	default_prepare
	# Requires unpackaged 'aioredis'
	rm -f test/test_aioredis*.py
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
694b1b540e8b9b32a5a44e3094b8bab307c5af400335efe9eb034c5ed161c1583d4700c08ef16991a1f03502aeb835d89b327448c3385de548dd39aafdbcc2a8  py3-fakeredis-2.10.3.tar.gz
"
