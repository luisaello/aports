# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=tracker
pkgver=3.5.0
pkgrel=0
pkgdesc="Personal search tool and storage system"
url="https://wiki.gnome.org/Projects/Tracker"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	asciidoc
	bash-completion
	dbus-dev
	glib-dev
	gobject-introspection-dev
	icu-dev
	json-glib-dev
	libsoup3-dev
	libstemmer-dev
	libxml2-dev
	meson
	py3-gobject3
	py3-setuptools
	sqlite-dev
	networkmanager-dev
	vala
	"
checkdepends="dbus"
subpackages="
	$pkgname-dbg
	$pkgname-dev
	$pkgname-doc
	$pkgname-lang
	lib$pkgname:libs
	$pkgname-bash-completion
	$pkgname-testutils:_testutils"
source="https://download.gnome.org/sources/tracker/${pkgver%.*}/tracker-$pkgver.tar.xz"

case "$CARCH" in
arm*|aarch64|x86)
	# segfaults in sparql tests
	options="$options !check"
	;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		-Ddocs=false \
		-Dsystemd_user_services=false \
		-Dtests="$(want_check && echo true || echo false)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	XDG_RUNTIME_DIR="$(mktemp -p "$builddir" -d)" \
	dbus-run-session -- \
	meson test -t 10 --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_testutils() {
	pkgdesc="$pkgname (test utilities)"
	depends="tracker=$pkgver-r$pkgrel py3-gobject3 bash"

	amove usr/lib/tracker-3.0/trackertestutils
}

sha512sums="
2c6d4f1c2c9d7687884052ae2ebc68e588cf91c56f98012ee138fc2020c66accb40f021c699d102b7634a936692341bd7ea28f3adfdb3c63c82ed9f837f97ba5  tracker-3.5.0.tar.xz
"
