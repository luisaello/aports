# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=eog
pkgver=44.0
pkgrel=1
pkgdesc="An image viewer for GNOME"
url="https://wiki.gnome.org/Apps/EyeOfGnome"
# s390x: blocked by exempi-dev
arch="all !s390x"
license="GPL-2.0-or-later"
depends="gsettings-desktop-schemas"
depends_dev="
	exempi-dev
	gnome-desktop-dev
	gvfs-dev
	jpeg-dev
	lcms2-dev
	libexif-dev
	libpeas-dev
	libportal-dev
	librsvg-dev
	libxml2-dev
	"
makedepends="
	$depends_dev
	itstool
	libhandy1-dev
	meson
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/eog/${pkgver%.*}/eog-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
8bd54b53100c0a8e596dec070f8a711fbc2a0caf396afb5b2d8ba45f86215c1823736a3b898d0460ac91204d1cf1a492ec993e66a025af167e3dc4aec3149b2d  eog-44.0.tar.xz
"
