# Contributor: Marvin Preuss <marvin@xsteadfastx.org>
# Maintainer: Marvin Preuss <marvin@xsteadfastx.org>
pkgname=upmpdcli
pkgver=1.7.7
pkgrel=0
pkgdesc="upmpdcli is a UPnP Media Renderer front-end for MPD, the Music Player Daemon"
pkgusers="upmpdcli"
pkggroups="upmpdcli"
url="https://www.lesbonscomptes.com/upmpdcli"
arch="all"
license="LGPL-2.1-or-later"
options="!check"
makedepends="libupnpp-dev libmpdclient-dev libmicrohttpd-dev jsoncpp-dev"
subpackages="$pkgname-doc $pkgname-openrc"
install="$pkgname.pre-install"
source="$pkgname-$pkgver.tar.gz::https://www.lesbonscomptes.com/upmpdcli/downloads/upmpdcli-$pkgver.tar.gz
	$pkgname.initd
	$pkgname.conf"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc
	make
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.conf "$pkgdir"/etc/$pkgname.conf
	install -d -o upmpdcli -g upmpdcli "$pkgdir"/var/cache/upmpdcli
}

sha512sums="
764606805f413643e1432d27f407ab3020d55e6c374210d56708ddd94285e364fdb266739d6f5d406e8f8708669820e4841449aebd2ea966974144ae90bfa38c  upmpdcli-1.7.7.tar.gz
f586c538abb51f373bf8a8277132694ad289f9c23de9389cb5c9cb67fdbec192529645b759f745ddeca3ce24893580b0ce0d344a8e75b3fd1edd021a0335f617  upmpdcli.initd
b6815c29e8c648fd7577f9137f1552dab96710e80458a19fc332c6382471c47fdc056c8e3e1353fc982610173e498b96df8fd18de93f5546773a22e02e58886d  upmpdcli.conf
"
