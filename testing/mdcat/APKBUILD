# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=mdcat
pkgver=2.0.0
pkgrel=0
pkgdesc="Cat for markdown"
url="https://github.com/swsnr/mdcat"
# error: Undefined temporary symbol .LBB17_2
arch="all !armhf !armv7"
license="Apache-2.0"
makedepends="asciidoctor cargo shared-mime-info openssl-dev>3"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/swsnr/mdcat/archive/refs/tags/mdcat-$pkgver.tar.gz"
builddir="$srcdir/mdcat-mdcat-$pkgver"

case "$CARCH" in
riscv64)
	options="$options textrels"
	;;
esac

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mdcat -t "$pkgdir"/usr/bin/
	ln -s /usr/bin/mdcat "$pkgdir"/usr/bin/mdless

	# Install the generated shell completion files
	install -Dm644 target/release/build/mdcat-*/out/completions/mdcat.bash \
		"$pkgdir"/usr/share/bash-completion/completions/mdcat
	install -Dm644 target/release/build/mdcat-*/out/completions/mdless.bash \
		"$pkgdir"/usr/share/bash-completion/completions/mdless
	install -Dm644 target/release/build/mdcat-*/out/completions/_mdcat \
		-t "$pkgdir"/usr/share/zsh/site-functions
	install -Dm644 target/release/build/mdcat-*/out/completions/_mdless \
		-t "$pkgdir"/usr/share/zsh/site-functions
	install -Dm644 target/release/build/mdcat-*/out/completions/mdcat.fish \
		-t "$pkgdir"/usr/share/fish/completions
	install -Dm644 target/release/build/mdcat-*/out/completions/mdless.fish \
		-t "$pkgdir"/usr/share/fish/completions

	# Install the generated man-page
	install -Dm644 "$builddir"/target/release/build/mdcat-*/out/mdcat.1 \
		-t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
54bd74e741ef1f6b542b2f28bdfedb0af10f5fd42b9abf0e3c073c23b18cba591d34c0a52501523479daa5235ce21709d750b6bf93040490055170439594b320  mdcat-2.0.0.tar.gz
"
