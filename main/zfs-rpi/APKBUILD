# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>

# when changing _ver we *must* bump _rel
_name=zfs
_ver=2.1.10
_rel=2
# when changing _kver/_krel make sure _rel=0
_flavor=${FLAVOR:-rpi}
_kpkg=linux-$_flavor
_kver=6.1.22
_krel=1

_kpkgver="$_kver-r$_krel"

# for custom kernels set $FLAVOR
case $CARCH in
	armhf|armv7) _extra_flavors="rpi2";;
	aarch64) _extra_flavors="rpi4";;
esac

_kpkg=linux-$_flavor
_kabi="$_kver-$_krel-$_flavor"

pkgname=$_name-$_flavor
pkgver=$_kver
pkgrel=$(( $_krel + $_rel ))

pkgdesc="ZFS Linux kernel modules"
url="https://openzfs.org"
arch="aarch64 armhf armv7"
license="CDDL-1.0"
depends="$_kpkg=$_kpkgver"
depends_dev="glib-dev e2fsprogs-dev util-linux-dev libtirpc-dev
	$_kpkg-dev=$_kpkgver"

makedepends="$depends_dev linux-headers xz"
install_if="zfs $_kpkg=$_kpkgver"
subpackages="$pkgname-dev"
source="https://github.com/openzfs/zfs/releases/download/zfs-$_ver/zfs-$_ver.tar.gz
	revert-sparse-file-break.patch
	"
builddir="$srcdir/$_name-$_ver"

for f in $_extra_flavors; do
	makedepends="$makedepends linux-$f-dev=$_kpkgver"
	subpackages="$subpackages zfs-$f:_extra"
done

prepare() {
	default_prepare
	# verify the kernel version
	local _kapkbuild="$startdir"/../../main/linux-$_flavor-/APKBUILD
	if [ -f $_kapkbuild ]; then
		(	. $_kapkbuild
			pkgname=$_name-$_flavor
			[ "$_kver" != "$pkgver" ] && die "please update _kver to $pkgver"
			[ "$_krel" != "$pkgrel" ] && die "please update _krel to $pkgrel"
			return 0
		)
	fi
}

build() {
	local flavor= kabi=
	for flavor in $_flavor $_extra_flavors; do
		kabi="$_kver-$_krel-$flavor"
		mkdir -p "$kabi"
		(cd $kabi
		../configure --prefix=/usr \
			--with-tirpc \
			--sysconfdir=/etc \
			--mandir=/usr/share/man \
			--infodir=/usr/share/info \
			--localstatedir=/var \
			--with-config=kernel \
			--with-linux=/usr/src/linux-headers-$kabi

		make
		)
	done
}

package() {
	local flavor= kabi=
	for flavor in $_flavor $_extra_flavors; do
		kabi="$_kver-$_krel-$flavor"
		(cd $kabi && make DESTDIR="$pkgdir" install)
	done
}

_extra() {
	local flavor=${subpkgname##*-}
	depends="linux-$flavor=$_kpkgver"
	install_if="zfs linux-$flavor=$_kpkgver"
	pkgdesc="ZFS Linux kernel modules for $flavor"

	kabi="$_kver-$_krel-$flavor"
	mkdir -p "$subpkgdir"/lib/modules
	mv "$pkgdir"/lib/modules/$kabi "$subpkgdir"/lib/modules
}

dev() {
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/src "$subpkgdir"/usr
	default_dev
}

sha512sums="
3189c6f822c7b6caba650a43f321114ef0bd42b72f566c44ba2400005c61cc3d420149a72520aed5b94494c51c35629a6f645273941774d964f2b61c31c366b2  zfs-2.1.10.tar.gz
474273cef96c0ff5355c2204466efb60cd32d862b72b43573f6d254312ab2b55f571bc9e2647c2e0ed95caa6387461875269ebcacadc8ed5ba444c01b88f9463  revert-sparse-file-break.patch
"
