# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=zfs
pkgver=2.1.10
pkgrel=1
pkgdesc="Advanced filesystem and volume manager"
url="https://openzfs.org"
arch="all"
license="CDDL-1.0"
provides="spl=$pkgver-r$pkgrel" # Mitigate upgrade conflicts
depends_dev="attr-dev e2fsprogs-dev glib-dev libtirpc-dev openssl-dev>3 util-linux-dev"
makedepends="$depends_dev linux-headers python3-dev py3-cffi py3-setuptools py3-distlib"
options="!check" # need to be run on live system w/ ZFS loaded.
subpackages="$pkgname-dev $pkgname-doc $pkgname-openrc $pkgname-libs $pkgname-dracut::noarch
	$pkgname-udev $pkgname-scripts $pkgname-utils-py:utils_py:noarch py3-pyzfs:pyzfs:noarch"
source="https://github.com/openzfs/zfs/releases/download/zfs-$pkgver/zfs-$pkgver.tar.gz
	dont-fsck-or-mount-umount-fstab-entries.patch
	revert-sparse-file-break.patch
	"

build() {
	export CFLAGS="$CFLAGS -fno-tree-vectorize"
	export CXXFLAGS="$CXXFLAGS -fno-tree-vectorize"
	export LIBS="$LIBS -lintl"
	./configure --prefix=/usr \
		--with-tirpc \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-config=user \
		--with-udevdir=/lib/udev \
		--disable-systemd \
		--disable-static \
		--with-python=3 \
		--enable-pyzfs
	make
}

package() {
	make DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/usr/share/initramfs-tools

	# Fix permissions of zfs OpenRC service configuration (#13840)
	chmod 644 "$pkgdir"/etc/conf.d/zfs
}

dracut() {
	pkgdesc="$pkgdesc (dracut)"

	amove usr/lib/dracut
}

udev() {
	pkgdesc="$pkgdesc (udev)"

	amove lib/udev
}

scripts() {
	pkgdesc="$pkgdesc (scripts)"

	amove usr/share/zfs
}

utils_py() {
	pkgdesc="$pkgdesc (python utils)"
	depends="python3"

	amove usr/bin/arc_summary
	amove usr/bin/arcstat
	amove usr/bin/dbufstat
}

pyzfs() {
	pkgdesc="$pkgdesc (Python lib to interact with ZFS)"
	depends="python3 $pkgname"

	amove usr/lib/python3*
}

sha512sums="
3189c6f822c7b6caba650a43f321114ef0bd42b72f566c44ba2400005c61cc3d420149a72520aed5b94494c51c35629a6f645273941774d964f2b61c31c366b2  zfs-2.1.10.tar.gz
3c2ffea6a6ea05a9953b965989198801d3528f281f9099401836fbdcdbae4f439ae65e3e68096ca7d9de7c39681aca3aa9cd71885fe14cfe17539fdd744a9925  dont-fsck-or-mount-umount-fstab-entries.patch
474273cef96c0ff5355c2204466efb60cd32d862b72b43573f6d254312ab2b55f571bc9e2647c2e0ed95caa6387461875269ebcacadc8ed5ba444c01b88f9463  revert-sparse-file-break.patch
"
